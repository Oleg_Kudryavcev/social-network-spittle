$(document).ready(function(){

    $('.likePost').click(function () {
        var postId = this.id;
        $.ajax({
            type: 'POST',
            url: "like/" + postId,

            success: function(likesCount) {
                $('#' + postId).val(likesCount);
            },
            error : function(e) {
            }
        }).fail(function(jqXHR) {
            alert('error ' + jqXHR.status + '  ' + jqXHR.text());
        });
    });

    $('.likePostUser').click(function () {
        var postId = this.id;
        $.ajax({
            type: 'POST',
            url: "/like/" + postId,

            success: function(likesCount) {
                $('#' + postId).val(likesCount);
            },
            error : function(e) {
            }
        }).fail(function(jqXHR) {
            alert('error ' + jqXHR.status + '  ' + jqXHR.text());
        });
    });

    $('.likePhoto').click(function () {
        var photoId = this.id;
        $.ajax({
            type: 'POST',
            url: "/photo_like/" + photoId,

            success: function(likesCount) {
                $('#' + photoId).val(likesCount);
            },
            error : function(e) {
            }
        }).fail(function(jqXHR) {
            alert('error ' + jqXHR.status + '  ' + jqXHR.text());
        });
    });
});