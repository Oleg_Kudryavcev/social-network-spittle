<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 10.02.15
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>" />
</head>
<body>

<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
        <%--<c:if test="${registration eq true}">--%>
            <%--<li>Spittle</li>--%>
        <%--</c:if>--%>
        <%--<c:if test="${registration != true}">--%>
            <li><a href="/posts">profile</a></li>
            <li><a href="/searchFriends">search friends</a></li>
            <li><a href="/logout">logout</a></li>
            <li><a href="/settings">settings</a></li>
            <li><a href="/friends">friends</a></li>
            <li><a href="/dialog">dialogs</a></li>
            <li><a href="/albums/${user.id}">my albums</a></li>
        <%--</c:if>--%>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text">
          <img src="/image/${user.id}" alt="" width="250" height="200"><br>
          <h4>Change avatar</h4>
          <form action="/download" method="post" enctype="multipart/form-data">
              <input type="file" name="file">
              <input type="submit" value="download">
          </form>
      </div>
        <div>
            <table id="userInformation">
                <tr>
                    <td>Name:</td>
                    <td>${user.firstName} ${user.lastName}</td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td>${user.currentCity}</td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td>${user.birthDate}</td>
                </tr>
            </table>
        </div>
    </div>
    <div id="cnt-right">
      <form action="/addInformation" method="post">
            <h4>Current city</h4>
            <input id="text-profile" type="text" name="currentCity" value="${userInfo.currentCity}">
            <h4>Birthday</h4>
            <input id="text-profile" type="date" name="birthDate" value="${userInfo.birthDate}">
            <h4>School</h4>
            <input id="text-profile" type="text" name="school" value="${userInfo.school}">
            <h4>University</h4>
            <input id="text-profile" type="text" name="university" value="${userInfo.university}">
            <h4>Work place</h4>
            <input id="text-profile" type="text" name="workPlace" value="${userInfo.workPlace}">
            <h4>Relationship status</h4>

          <table id="userInformation">
              <tr>
                  <c:if test="${userInfo.relationshipStatus != null}">
                      <td>${userInfo.relationshipStatus}</td>
                  </c:if>
                  <td>
                      <select name="relationshipStatus">
                          <option value="-none selected-">-none selected-</option>
                          <option value="Single">Single</option>
                          <option value="Married">Married</option>
                          <option value="In love">In love</option>
                          <option value="Actively searching">Actively searching</option>
                      </select>
                  </td>
              </tr>
              </table>
          <h4>Sex</h4>
          <table id="userInformation">
              <tr>
                  <c:if test="${userInfo.sex != null}">
                      <td>${userInfo.sex}</td>
                  </c:if>
                  <td>
                      <select name="sex">
                          <option value="Male">Male</option>
                          <option value="Famale">Famale</option>
                      </select>
                  </td>
              </tr>
          </table> <br>
            <input id="submit-profile" type="submit" value="save">
            <c:if test="${registration eq true}">
              <td><a id="submit-profile" href="/next">next</a></td>
            </c:if>
      </form>
    </div>
  </div>
  <div id="footer"><h5>Spittle</h5></div>
</div>

</body>
</html>
