<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 11.02.15
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Spittle</title>
  <meta charset="utf-8" />
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <title></title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css" />
</head>
<body>
<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
      <li><a href="/posts">profile</a></li>
      <li><a href="/searchFriends">search friends</a></li>
      <li><a href="/logout">logout</a></li>
      <li><a href="/settings">settings</a></li>
      <li><a href="/friends">friends</a></li>
      <li><a href="/dialog">dialogs</a></li>
      <li><a href="/albums/${user.id}">my albums</a></li>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text"><img src="/image/${user.id}" alt="" width="250" height="200"></div>
      <div>
        <table id="userInformation">
          <tr>
            <td>Name:</td>
            <td>${user.firstName} ${user.lastName}</td>
          </tr>
          <tr>
            <td>City:</td>
            <td>${user.currentCity}</td>
          </tr>
          <tr>
            <td>Birthday:</td>
            <td>${user.birthDate}</td>
          </tr>
        </table>
      </div>
    </div>
    <div id="cnt-right">

        <c:if test="${! empty requestToFriend}">
          <h4>Next people want add you to friend:</h4><br>
          <table>
            <c:forEach items="${requestToFriend}" var="user">
              <tr>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
              </tr>
            </c:forEach>
          </table>
          <c:if test="${! empty listFriends}">
            <c:forEach items="${listFriends}" var="user">
              <a id="submit-profile" href="/confirmation?userId=${user.userId}&friendId=${user.friendId}">add</a>
            </c:forEach>
          </c:if>
        </c:if>

      <c:if test="${! empty friends}">
        <table>
          <c:forEach items="${friends}" var="friend">
            <tr>
              <td><img src="/image/${friend.id}" alt="" width="100" height="100"></td>
              <td><a href="/user/${friend.id}">${friend.firstName} ${friend.lastName}</a></td>
              <td></td>
              <td><a href="/chat/${friend.id}">chat</a></td>
              <td><a href="/remove/${friend.id}">remove from friends</a></td>
            </tr>
          </c:forEach>
        </table>
      </c:if>

    </div>
  </div>
  <div id="footer"><h5>Spittle</h5></div>
</div>
</body>
</html>