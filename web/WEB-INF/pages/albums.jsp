<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 25.02.15
  Time: 9:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css" />
  <link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.css" />
</head>
<body>

<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
      <li><a href="/posts">profile</a></li>
      <li><a href="/searchFriends">search friends</a></li>
      <li><a href="/logout">logout</a></li>
      <li><a href="/settings">settings</a></li>
      <li><a href="/friends">friends</a></li>
      <li><a href="/dialog">dialogs</a></li>
      <li><a href="/albums/${user.id}">albums</a></li>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text"><img src="/image/${user.id}" alt="" width="250" height="200"></div>
      <div>
        <table id="userInformation">
          <tr>
            <td>Name:</td>
            <td>${user.firstName} ${user.lastName}</td>
          </tr>
          <tr>
            <td>City:</td>
            <td>${user.currentCity}</td>
          </tr>
          <tr>
            <td>Birthday:</td>
            <td>${user.birthDate}</td>
          </tr>
        </table>
      </div>
    </div>
    <div id="cnt-right">

      <c:if test="${createAlbum eq true}">
        <form action="/album/add" method="post" id="postForm">
          <h4>Album name</h4>
          <input type="text" name="albumName"><br>
          <h4>Comment available</h4>
          <input type="radio" name="comment" value="friends" checked>friends<br>
          <input type="radio" name="comment" value="all">all users<br>
          <input type="radio" name="comment" value="nobody">no body<br>
          <input id="submit-profile" type="submit" value="create">
        </form>
      </c:if>
      <br><br>
      <c:if test="${!empty albums}">
        <table id="userInformation">
          <c:forEach items="${albums}" var="album">
            <tr>
              <td><a href="/album/${user.id}/${album.id}">${album.albumName}</a></td>
              <td>${album.countOfPhotos}</td>
            </tr>
          </c:forEach>
        </table>
      </c:if>

    </div>
  </div>
  <div id="footer"><h5>Spittle</h5></div>
</div>


</body>
</html>
