<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 11.02.15
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css" />
  <script src="/resources/js/jquery-2.1.3.min%20.js"></script>
  <script src="/resources/js/script.js"></script>
</head>
<body>

<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
      <li><a href="/posts">profile</a> </li>
      <li><a href="/searchFriends">search friends</a></li>
      <li><a href="/logout">logout</a></li>
      <li><a href="/settings">settings</a></li>
      <li><a href="/friends">friends</a></li>
      <li><a href="/dialog">dialogs</a></li>
      <li><a href="/albums/${id}">user albums</a></li>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text"><img src="/image/${id}" alt="" width="250" height="200"></div>
    </div>
    <div id="cnt-right">
      <form id="addPostForm" action="/newUserPost/${id}" method="post">
        <table>
          <tr>
            <td><textarea cols="30" rows="4" name="post"></textarea></td>
          </tr>
          <tr>
            <td><input id="submit-profile" type="submit" value="add"></td>
          </tr>
        </table>
      </form>
      <c:if test="${! empty posts}">
        <c:forEach items="${posts}" var="post">
          <table id="postsTable">
            <tr>
              <td>Who posted:</td>
              <td>${post.whoPosted}</td>
            </tr>
            <tr>
              <td>Post:</td>
              <td>${post.post}</td>
            </tr>
            <tr>
              <td>Posted time:</td>
              <td>${post.postedTime}</td>
            </tr>
            <tr>
              <td>Likes:</td>
              <td>
                <input class="likePostUser" id="${post.id}" type="button" value="${post.likes}">
              </td>
            </tr>
          </table>
        </c:forEach>
      </c:if>
    </div>
  </div>
  <div id="footer"><h5>Lorem ipsum dolor sit amet.</h5></div>
</div>
</body>
</html>
