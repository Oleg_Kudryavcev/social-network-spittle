<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Spittle</title>
    <script src="<c:url value="/resources/js/jquery-2.1.3.min%20.js"/>"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>" />
    <script src="<c:url value="/resources/js/sockjs-0.3.4.js"/>"></script>
    <script src="<c:url value="/resources/js/stomp.js"/>"></script>
    <script>

        var stompClient = null;

        function connect() {
            var socket = new SockJS('/add');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function(frame) {
                console.log('Connected: ' + frame);
                stompClient.subscribe('/user/queue/showResult', function(message) {
                    showResult(message.body);
                });
            });
        }

        function disconnect() {
            stompClient.disconnect();
            console.log("Disconnected");
        }

        function sendMessage() {
            var message = $('#message').val();
            var friendId = $('#friendId').val();
            $('#message').val('');
            $('#chat').append('[${user.firstName}] ' + message + '<br>');
            stompClient.send("/chat/add", {}, JSON.stringify({ 'message': message, 'friendId': friendId }));
        }

        function showResult(message) {
            $('#chat').append('${friend.firstName} ' + message + '<br>');
        }
    </script>

</head>
<body onload="connect()">

<div id="wr-container" class="border-radius-4">
    <div id="navbar">
        <ul class="nav-user">
            <li><a href="/posts">profile</a></li>
            <li><a href="/searchFriends">search friends</a></li>
            <li><a href="/logout">logout</a></li>
            <li><a href="/settings">settings</a></li>
            <li><a href="/friends">friends</a></li>
            <li><a href="/dialog">dialogs</a></li>
            <li><a href="/albums/${user.id}">my albums</a></li>
        </ul>
    </div>
    <div id="container-text" class="clearfix">
        <div id="cnt-left">
            <table id="userInformation">
                <tr>
                    <td>
                        <img src="/image/${user.id}" height="150" width="150">
                    </td>
                    <td>${user.firstName} ${user.lastName}</td>
                </tr>
                <tr>
                    <td>
                        <img src="/image/${friend.id}" height="150" width="150">
                    </td>
                    <td><a href="/user/${friend.id}">${friend.firstName} ${friend.lastName}</a></td>
                </tr>
            </table>
        </div>
        <div id="cnt-right">
            <div id="chat">
                <c:forEach items="${chatHistory}" var="chatMessage">
                    <c:if test="${chatMessage.whoWriteId eq user.id}">
                        [${user.firstName} ${chatMessage.writeIn}]
                         ${chatMessage.message}<br><br>
                    </c:if>
                    <c:if test="${chatMessage.whoWriteId != user.id}">
                        [${friend.firstName} ${chatMessage.writeIn}]
                         ${chatMessage.message}<br><br>
                    </c:if>
                </c:forEach>
            </div>
            <br/>
            <form id="chatForm">
                    <textarea cols="30" rows="5" id="message"></textarea>
                    <input type="number" id="friendId" value="${friend.id}" style="visibility: hidden">
                    <input type="button" onclick="sendMessage()" value="send" style="margin: 0 auto;padding: 5px 0;">
            </form>
        </div>
    </div>
    <div id="footer"><h5>Spittle</h5></div>
</div>
</body>
</html>