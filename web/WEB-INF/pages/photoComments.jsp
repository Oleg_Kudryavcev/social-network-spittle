<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 03.03.15
  Time: 22:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <link rel="stylesheet" type="text/css" href="/resources/css/style.css" />
</head>
<body>

<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
      <li><a href="/posts">profile</a></li>
      <li><a href="/searchFriends">search friends</a></li>
      <li><a href="/logout">logout</a></li>
      <li><a href="/settings">settings</a></li>
      <li><a href="/friends">friends</a></li>
      <li><a href="/dialog">dialogs</a></li>
      <li><a href="/albums/${user.id}">albums</a></li>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text"><img src="/image/${user.id}" alt="" width="250" height="200"></div>
      <div>
        <table id="userInformation">
          <tr>
            <td>Name:</td>
            <td>${user.firstName} ${user.lastName}</td>
          </tr>
          <tr>
            <td>City:</td>
            <td>${user.currentCity}</td>
          </tr>
          <tr>
            <td>Birthday:</td>
            <td>${user.birthDate}</td>
          </tr>
        </table>
      </div>
    </div>
    <div id="cnt-right">
      <img src="/photo/${photoId}/${userId}">
      <c:if test="${! empty comments}">
      <table>
        <c:forEach items="${comments}" var="comment">
        <tr>
          <td>${comment.comment}</td>
        </tr>
        <tr>
          <td>${comment.nameWhoComment}</td>
        </tr>
        <tr>
          <td>${comment.commentDate}</td>
        </tr>
        <tr>
          <td>----------------------------------</td>
        </tr>
        </c:forEach>
        </c:if>
        <form action="/comment/${photoId}/${userId}/${albumId}" method="post">
          <tr>
            <td>
              <textarea cols="30" rows="4" name="photo_comment"></textarea>
            </td>
          </tr>
          <tr>
            <td><input type="submit" value="comment"></td>
          </tr>
        </form>
        </table>
    </div>
  </div>
  <div id="footer"><h5>Spittle</h5></div>
</div>


</body>
</html>
