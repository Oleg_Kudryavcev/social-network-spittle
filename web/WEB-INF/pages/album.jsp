<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 25.02.15
  Time: 9:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css" />
  <script src="<c:url value="/resources/js/jquery-2.1.3.min%20.js"/>"></script>
  <script src="<c:url value="/resources/js/script.js"/>"></script>
</head>
<body>

<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
      <li><a href="/posts">profile</a></li>
      <li><a href="/searchFriends">search friends</a></li>
      <li><a href="/logout">logout</a></li>
      <li><a href="/settings">settings</a></li>
      <li><a href="/friends">friends</a></li>
      <li><a href="/dialog">dialogs</a></li>
      <li><a href="/albums/${user.id}">albums</a></li>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text"><img src="/image/${user.id}" alt="" width="250" height="200"></div>
      <div>
        <table id="userInformation">
          <tr>
            <td>Name:</td>
            <td>${user.firstName} ${user.lastName}</td>
          </tr>
          <tr>
            <td>City:</td>
            <td>${user.currentCity}</td>
          </tr>
          <tr>
            <td>Birthday:</td>
            <td>${user.birthDate}</td>
          </tr>
        </table>
      </div>
    </div>
    <div id="cnt-right">


      <c:if test="${addPhoto eq true}">
        <form action="/save/${albumId}" method="post" enctype="multipart/form-data">
          <table>
            <tr>
              <td>
                <input id="text-profile" type="text" name="description" placeholder="description">
              </td>
            </tr>
            <tr>
              <td>
                <input id="file" type="file" value="select photo" name="photo">
              </td>
            </tr>
            <tr>
              <td><input id="submit-profile" type="submit" value="save"></td>
            </tr>
          </table>
        </form>
      </c:if>


      <c:if test="${! empty photos}">
        <table>
          <c:forEach items="${photos}" var="photo">
            <tr>
              <td><img src="/photo/${photo.id}/${userId}"></td>
            </tr>
            <tr>
              <td>${photo.description}</td>
            </tr>
            <tr>
              <td>like</td>
              <td><input class="likePhoto" id="${photo.id}" type="button" value="${photo.likes}"></td>
            </tr>
            <c:if test="${commentAvailable eq true}">
              <tr>
                <td><a href="/comments/${photo.id}/${user.id}/${albumId}">show comments</a></td>
              </tr>
            </c:if>
          </c:forEach>
        </table>
      </c:if>

    </div>
  </div>
  <div id="footer"><h5>Spittle</h5></div>
</div>

</body>
</html>
