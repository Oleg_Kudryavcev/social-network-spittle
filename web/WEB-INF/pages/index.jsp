<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 16.01.15
  Time: 9:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page session="false" %>
<html>
  <head>
    <title>Social Network</title>
      <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
      <link href="/resources/css/style.css" rel="stylesheet" type="text/css">
      <script src="<c:url value="/resources/js/script.js"/>"></script>
  </head>
  <body>

  <div id="wr-container" class="border-radius-4">
    <div id="navbar">
      <ul id="nav-title">
        <li id="title">Spittle - social network for those who like communicate</li>
      </ul>
    </div>
    <div id="container-text" class="clearfix">
      <div id="cnt-left">
        <h3>Registration</h3>
        <form action="/registration" method="post">
          <h4>First Name</h4>
          <input type="text" name="firstName" id="text-profile" value="${user.firstName}">
          <h4>Last Name</h4>
          <input type="text" name="lastName" id="text-profile" value="${user.lastName}">
          <h4>Email</h4>
          <c:if test="${email eq true}">
            <h4 style="color: red">This email is already exist.</h4>
          </c:if>
          <input type="email" name="email" id="text-profile" value="${user.email}">
          <h4>Password</h4>
          <input type="password" name="password" id="text-profile">
          <input type="submit" name="registration" id="submit-profile" value="Registration">
        </form>
      </div>
      <div id="cnt-right">
        <h3>LogIn</h3>
        <form action="/login" method="post">
          <h4>Email</h4>
          <c:if test="${error eq true}">
            <h4 style="color:#ff0000;">invalid email or password</h4>
          </c:if>
          <input type="email" name="username" id="text-profile" value="${email}">
          <h4>Password</h4>
          <input type="password" name="password" id="text-profile">
          <input type="submit" name="input-profile" id="submit-profile" value="login">
          <input type="hidden" name="${_csrf.parameterName}"
                 value="${_csrf.token}" />
        </form>
      </div>
    </div>
    <div id="footer"></div>
  </div>
  </body>

</html>
