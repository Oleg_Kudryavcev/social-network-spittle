<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleh
  Date: 09.02.15
  Time: 20:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <link rel="stylesheet" type="text/css" href="/resources/css/style.css" />
</head>
<body>

<div id="wr-container" class="border-radius-4">
  <div id="navbar">
    <ul class="nav-user">
      <li><a href="/posts">profile</a></li>
      <li><a href="/searchFriends">search friends</a></li>
      <li><a href="/logout">logout</a></li>
      <li><a href="/settings">settings</a></li>
      <li><a href="/friends">friends</a></li>
      <li><a href="/dialog">dialogs</a></li>
      <li><a href="/albums/${user.id}">albums</a></li>
    </ul>
  </div>
  <div id="container-text" class="clearfix">
    <div id="cnt-left">
      <div class="img-text"><img src="/image/${user.id}" alt="" width="250" height="200"></div>
      <div>
        <table id="userInformation">
          <tr>
            <td>Name:</td>
            <td>${user.firstName} ${user.lastName}</td>
          </tr>
          <tr>
            <td>City:</td>
            <td>${user.currentCity}</td>
          </tr>
          <tr>
            <td>Birthday:</td>
            <td>${user.birthDate}</td>
          </tr>
        </table>
      </div>
    </div>
    <div id="cnt-right">
      <form action="/search" method="post">
        <h4>Name</h4>
        <input id="text-profile" type="text" name="name" placeholder="Type first or last name of person">
        <input id="submit-profile" type="submit" value="search">
        <br><br>
      </form>
      <form action="/extraSearching" method="post">
        <h4>City</h4>
        <input id="text-profile" type="text" name="city">
        <h4>School</h4>
        <input id="text-profile" type="text" name="school">
        <h4>University</h4>
        <input id="text-profile" type="text" name="university">
        <input id="submit-profile" type="submit" value="search">

        <br><br><br>
        <c:if test="${! empty findUsers}">
          <ul>
            <c:forEach items="${findUsers}" var="findUser">
              <li><h4>${findUser.firstName} ${findUser.lastName}</h4>
                <ul>
                  <li><a id="submit-profile" href="/add_to_friends?id=${findUser.id}">add to friends</a></li><br><br>
                  <li><a id="submit-profile" href="/user/${findUser.id}">page</a></li><br><br>
                  <li><a id="submit-profile" href="/chat/${findUser.id}">chat</a></li>
                </ul>
              </li>
              <br><br><br><br>
            </c:forEach>
          </ul>
        </c:if>
      </form>
    </div>
  </div>
  <div id="footer"><h5>Spittle</h5></div>
</div>


</body>
</html>
