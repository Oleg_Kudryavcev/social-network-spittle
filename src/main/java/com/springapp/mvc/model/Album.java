package com.springapp.mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by oleh on 24.02.15.
 */
@Entity
@Table(name = "album_info")
public class Album {

    @Id
    @GeneratedValue
    private int id;
    private String albumName;
    private int countOfPhotos;
    private int userId;
    private boolean openAlbum;
    private String whoCanComment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public int getCountOfPhotos() {
        return countOfPhotos;
    }

    public void setCountOfPhotos(int countOfPhotos) {
        this.countOfPhotos = countOfPhotos;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isOpenAlbum() {
        return openAlbum;
    }

    public void setOpenAlbum(boolean openAlbum) {
        this.openAlbum = openAlbum;
    }

    public String getWhoCanComment() {
        return whoCanComment;
    }

    public void setWhoCanComment(String whoCanComment) {
        this.whoCanComment = whoCanComment;
    }
}
