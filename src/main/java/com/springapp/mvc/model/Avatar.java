package com.springapp.mvc.model;

import javax.persistence.*;
import java.sql.Blob;

/**
 * Created by oleh on 24.02.15.
 */
@Entity
@Table(name = "avatar")
public class Avatar {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;
    @Column(name = "FILENAME")
    private String fileName;
    @Lob
    @Column(name = "IMAGE")
    private Blob content;
    @Column(name = "CONTENT_TYPE")
    private String contentType;
    @Column(name = "USER_ID")
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Blob getContent() {
        return content;
    }

    public void setContent(Blob content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
