package com.springapp.mvc.model;

import javax.persistence.*;
import java.sql.Blob;

/**
 * Created by oleh on 25.02.15.
 */
@Entity
@Table(name = "album_photo")
public class AlbumPhoto {

    @Id
    @GeneratedValue
    private int id;
    @Lob
    private Blob image;
    private String description;
    private int likes;
    private int whomPhotoId;
    private int albumId;
    private String contentType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getWhomPhotoId() {
        return whomPhotoId;
    }

    public void setWhomPhotoId(int whomPhotoId) {
        this.whomPhotoId = whomPhotoId;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
