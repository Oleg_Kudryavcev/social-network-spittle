package com.springapp.mvc.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by oleh on 09.02.15.
 */
@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue
    private int id;
    private int user_id;
    private String whoPosted;
    private String post;
    private Date postedTime;
    private int likes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getWhoPosted() {
        return whoPosted;
    }

    public void setWhoPosted(String whoPosted) {
        this.whoPosted = whoPosted;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Date getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(Date postedTime) {
        this.postedTime = postedTime;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
