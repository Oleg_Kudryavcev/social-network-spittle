package com.springapp.mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by oleh on 09.03.15.
 */
@Entity
@Table(name = "photo_like")
public class PhotoLike {

    @Id
    @GeneratedValue
    private int id;
    private int whoLikeId;
    private int photoId;

    public PhotoLike(int whoLikeId, int photoId) {
        this.whoLikeId = whoLikeId;
        this.photoId = photoId;
    }

    public PhotoLike() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWhoLikeId() {
        return whoLikeId;
    }

    public void setWhoLikeId(int whoLikeId) {
        this.whoLikeId = whoLikeId;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
