package com.springapp.mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by oleh on 25.02.15.
 */
@Entity
@Table(name = "photo_comments")
public class PhotoComment {

    @Id
    @GeneratedValue
    private int id;
    private String comment;
    private int whoCommentId;
    private String nameWhoComment;
    private int photoId;
    private int likes;
    private Date commentDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getWhoCommentId() {
        return whoCommentId;
    }

    public void setWhoCommentId(int whoCommentId) {
        this.whoCommentId = whoCommentId;
    }

    public String getNameWhoComment() {
        return nameWhoComment;
    }

    public void setNameWhoComment(String nameWhoComment) {
        this.nameWhoComment = nameWhoComment;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }
}
