package com.springapp.mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by oleh on 09.03.15.
 */
@Entity
@Table(name = "post_like")
public class PostLike {

    @Id
    @GeneratedValue
    private int id;
    private int userWhoLikeId;
    private int postId;

    public PostLike(int userWhoLikeId, int postId) {
        this.userWhoLikeId = userWhoLikeId;
        this.postId = postId;
    }

    public PostLike() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserWhoLikeId() {
        return userWhoLikeId;
    }

    public void setUserWhoLikeId(int userWhoLikeId) {
        this.userWhoLikeId = userWhoLikeId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
