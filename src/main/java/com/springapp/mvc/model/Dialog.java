package com.springapp.mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by oleh on 17.02.15.
 */
@Entity
@Table(name = "dialog")
public class Dialog {

    @Id
    @GeneratedValue
    private int id;
    private int whoWriteId;
    private int whoAnswerId;
    private String message;
    private Date writeIn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWhoWriteId() {
        return whoWriteId;
    }

    public void setWhoWriteId(int whoWriteId) {
        this.whoWriteId = whoWriteId;
    }

    public int getWhoAnswerId() {
        return whoAnswerId;
    }

    public void setWhoAnswerId(int whoAnswerId) {
        this.whoAnswerId = whoAnswerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getWriteIn() {
        return writeIn;
    }

    public void setWriteIn(Date writeIn) {
        this.writeIn = writeIn;
    }
}
