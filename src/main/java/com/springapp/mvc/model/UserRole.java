package com.springapp.mvc.model;

import javax.persistence.*;

/**
 * Created by oleh on 23.02.15.
 */
@Entity
@Table(name = "user_roles")
public class UserRole {

    @Id
    @GeneratedValue
    @Column(name = "user_role_id")
    private int userRoleId;
    @Column(name = "user_email")
    private String userEmail;
    private String role;

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
