package com.springapp.mvc.controllers;

import com.springapp.mvc.dao.image.albom.AlbumPhotoDAO;
import com.springapp.mvc.model.*;
import com.springapp.mvc.service.friend.FriendService;
import com.springapp.mvc.service.image.album.AlbumInfoService;
import com.springapp.mvc.service.image.album.AlbumPhotoService;
import com.springapp.mvc.service.image.avatar.AvatarService;
import com.springapp.mvc.service.user.UserService;
import com.springapp.mvc.util.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

/**
 * Created by oleh on 24.02.15.
 */
@Controller
public class AlbumController {

    @Autowired
    private AlbumInfoService albumInfoService;
    @Autowired
    private AlbumPhotoService albumPhotoService;
    @Autowired
    private AvatarService avatarService;
    @Autowired
    private FriendService friendService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/albums/{userId}", method = RequestMethod.GET)
    public String albums(@PathVariable int userId,
                         ModelMap modelMap) {
        User user = userService.getUserInfo(userId);
        Avatar avatar = avatarService.getAvatar(userId);
        if (user.getId() == userId) {
            modelMap.addAttribute("createAlbum", true);
        }
        if (avatar != null) {
            modelMap.addAttribute("imageConsist", true);
        }
        modelMap.addAttribute("albums", albumInfoService.getAlbums(userId));
        modelMap.addAttribute("user", user);
        return "albums";
    }

    @RequestMapping(value = "/album/add", method = RequestMethod.POST)
    public String addAlbum(@RequestParam String albumName,
                           @RequestParam String comment,
                           Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        albumInfoService.createAlbum(albumName, user.getId(), comment);
        return "redirect:/albums/" + user.getId();
    }

    @RequestMapping(value = "/album/remove/{id}", method = RequestMethod.GET)
    public String removeAlbum(@PathVariable int id) {
        albumInfoService.removeAlbum(
                albumInfoService.getAlbumById(id)
        );
        return "redirect:/albums";
    }

    @RequestMapping(value = "/album/{userId}/{albumId}", method = RequestMethod.GET)
    public String album(@PathVariable int userId,
                        @PathVariable int albumId,
                        ModelMap modelMap,
                        Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        Album album = albumInfoService.getAlbumById(albumId);
        if (album.getWhoCanComment().equals("friends")) {
            for (Friend friend : friendService.getFriends(userId)) {
                if (friend.getFriendId() == user.getId()) {
                    modelMap.addAttribute("commentAvailable", true);
                    break;
                }
            }
        } else if (album.getWhoCanComment().equals("all")) {
            modelMap.addAttribute("commentAvailable", true);
        }
        List<AlbumPhoto> albumPhotos = albumPhotoService.getAllUserPhotos(albumId);
        for (AlbumPhoto albumPhoto : albumPhotos) {
            albumPhoto.setLikes(albumPhotoService.getLikes(albumPhoto.getId()));
        }
        modelMap.addAttribute("albumId", albumId);
        modelMap.addAttribute("photos", albumPhotos);
        modelMap.addAttribute("userId", userId);
        if (user.getId() == userId) {
            modelMap.addAttribute("addPhoto", true);
        }
        modelMap.addAttribute("user", userService.getUserInfo(userId));
        return "album";
    }

    @RequestMapping(value = "/comments/{photoId}/{userId}/{albumId}", method = RequestMethod.GET)
    public String photoComment(@PathVariable int photoId,
                               @PathVariable int userId,
                               @PathVariable int albumId,
                               ModelMap modelMap) {
        List<PhotoComment> photoComments =  albumPhotoService.allPhotoComments(photoId);
        Collections.sort(photoComments, new Comparator<PhotoComment>() {
            @Override
            public int compare(PhotoComment o1, PhotoComment o2) {
                if (o1.getCommentDate().getTime() < o2.getCommentDate().getTime()) {
                    return 1;
                } else if (o1.getCommentDate().getTime() == o2.getCommentDate().getTime()) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        modelMap.addAttribute("comments", photoComments);
        modelMap.addAttribute("userId", userId);
        modelMap.addAttribute("albumId", albumId);
        modelMap.addAttribute("photoId", photoId);
        modelMap.addAttribute("user", userService.getUserInfo(userId));
        return "photoComments";
    }

    @RequestMapping(value = "/photo/{photoId}/{userId}", method = RequestMethod.GET)
    public String showPhotos(@PathVariable final int photoId,
                             @PathVariable final int userId,
                             HttpServletResponse response) {
        AlbumPhoto albumPhoto = albumPhotoService.getAlbumPhotoById(userId, photoId);
        response.setContentType("image/jpeg, image/png, image/jpg");
        Image.writeFileContentHttpResponse(albumPhoto.getImage(), response);
        return "album";
    }

    @RequestMapping(value = "/save/{albumId}", method = RequestMethod.POST)
    public String savePhoto(
            @ModelAttribute("albumPhoto") AlbumPhoto albumPhoto,
            @PathVariable int albumId,
            @RequestParam("photo") MultipartFile multipartFile,
            Principal principal,
            @RequestParam String description
    ) {
        User user = userService.findUserByEmail(principal.getName());
        try {
            albumPhotoService.createAlbumPhoto(multipartFile.getInputStream(), description,
                    multipartFile.getContentType(), user.getId(), albumId, multipartFile.getSize(), albumPhoto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/album/" + user.getId() + "/" + albumId;
    }

    @RequestMapping(value = "/photo_like/{photoId}", method = RequestMethod.POST)
    public @ResponseBody String like(@PathVariable int photoId,
                                     Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        if (albumPhotoService.getWhoLike(user.getId(), photoId) == null ) {
            albumPhotoService.likePhoto(user.getId(), photoId);
        }
        return String.valueOf(albumPhotoService.getLikes(photoId));
    }

    @RequestMapping(value = "/comment/{photoId}/{userId}/{albumId}", method = RequestMethod.POST)
    public String addCommentToPhoto(@PathVariable int photoId,
                                    @RequestParam("photo_comment") String comment,
                                    @PathVariable int userId,
                                    @PathVariable int albumId,
                                    Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        albumPhotoService.createPhotoComment(comment, user.getId(), photoId, user.getFirstName());
        return "redirect:/comments/" + photoId + "/" + userId + "/" + albumId;
    }

}
