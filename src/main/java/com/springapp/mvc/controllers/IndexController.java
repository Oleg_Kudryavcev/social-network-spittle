package com.springapp.mvc.controllers;

import com.springapp.mvc.model.User;
import com.springapp.mvc.model.UserRole;
import com.springapp.mvc.service.user.UserDetailsServiceImplementation;
import com.springapp.mvc.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by oleh on 16.01.15.
 */
@Controller
public class IndexController {

    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String printWelcome(Principal principal) {
        if (principal != null) {
            return "redirect:/posts";
        } else {
            return "index";
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@RequestParam String firstName,
                               @RequestParam String lastName,
                               @RequestParam String password,
                               @RequestParam String email,
                               ModelMap modelMap) {
        User user = userService.findUserByEmail(email);
        if (user == null) {
            userService.createUser(firstName, lastName, password, email);
            User user1 = userService.getUserH(email, password);
            try {
                UserDetails userDetails = userDetailsService.loadUserByUsername(user1.getEmail());
                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                        userDetails, password, userDetails.getAuthorities()
                );
                authenticationManager.authenticate(auth);
                if(auth.isAuthenticated()) {
                    SecurityContextHolder.getContext().setAuthentication(auth);
                    modelMap.addAttribute("registration", true);
                    return "settings";
                }
            } catch (Exception e) {
                System.out.println("problem to authenticate user " + user1.getEmail() + e);
            }
        }
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("email", true);
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap modelMap,
                        @RequestParam(value = "error", required = false) String error) {
        if (error != null) {
            modelMap.addAttribute("error", true);
            return "index";
        }
        return "redirect:/posts";
    }

    @RequestMapping(value = "/logout/success", method = RequestMethod.GET)
    public String logout() {
        return "index";
    }

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String settings(Principal principal, ModelMap modelMap) {
        User user = userService.findUserByEmail(principal.getName());
        modelMap.addAttribute("userInfo", userService.getUserInfo(user.getId()));
        if (modelMap.get("registration") != null) {
            modelMap.addAttribute("registration", true);
        }
        modelMap.addAttribute("user", user);
        return "settings";
    }

    @RequestMapping(value = "/addInformation", method = RequestMethod.POST)
    public String addInfoAboutUser(@RequestParam String currentCity,
                                   @RequestParam Date birthDate,
                                   @RequestParam String school,
                                   @RequestParam String university,
                                   @RequestParam String workPlace,
                                   @RequestParam String sex,
                                   @RequestParam String relationshipStatus,
                                   Principal principal,
                                   ModelMap modelMap) {
        User user = userService.findUserByEmail(principal.getName());
        user.setCurrentCity(currentCity);
        user.setBirthDate(birthDate);
        user.setSchool(school);
        user.setUniversity(university);
        user.setWorkPlace(workPlace);
        user.setSex(sex);
        user.setRelationshipStatus(relationshipStatus);
        userService.updateUser(user);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("userInfo", userService.getUserInfo(user.getId()));
        return "settings";
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    public String nextPage() {
        return "redirect:/posts";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

}
