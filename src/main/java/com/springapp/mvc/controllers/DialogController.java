package com.springapp.mvc.controllers;

import com.springapp.mvc.model.Dialog;
import com.springapp.mvc.model.User;
import com.springapp.mvc.service.user.UserService;
import com.springapp.mvc.service.dialog.DialogService;
import com.springapp.mvc.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

/**
 * Created by oleh on 17.02.15.
 */
@Controller
public class DialogController {

    @Autowired
    private DialogService dialogService;
    @Autowired
    private UserService userService;
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @RequestMapping(value = "/dialog", method = RequestMethod.GET)
    public String dialog(ModelMap modelMap, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        modelMap.addAttribute("dialogs", dialogService.getDialogs(user.getId()));
        modelMap.addAttribute("user", user);
        return "dialog";
    }

    @RequestMapping(value = "/chat/{userId}", method = RequestMethod.GET)
    public String toConversation(@PathVariable int userId,
                                 ModelMap modelMap,
                                 Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        modelMap.addAttribute("friend", userService.getUserInfo(userId));
        modelMap.addAttribute("user", user);
        List<Dialog> chatHistory = dialogService.allDialogsWithFriend(user.getId(), userId);
        Collections.sort(chatHistory, new Comparator<Dialog>() {
            @Override
            public int compare(Dialog o1, Dialog o2) {
                if (o1.getWriteIn().getTime() < o2.getWriteIn().getTime()) {
                    return -1;
                } else if (o1.getWriteIn().getTime() == o2.getWriteIn().getTime()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        modelMap.addAttribute("chatHistory", chatHistory);
        return "chat";
    }

    @MessageMapping("/add")
    public void sendMessage(Message message, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        User friend = userService.getUserInfo(message.getFriendId());
        dialogService.createMessage(user.getId(), message.getFriendId(), message.getMessage());
        messagingTemplate.convertAndSendToUser(friend.getEmail(), "/queue/showResult", message.getMessage());
    }


}
