package com.springapp.mvc.controllers;

import com.springapp.mvc.model.Avatar;
import com.springapp.mvc.model.Friend;
import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.User;
import com.springapp.mvc.service.friend.FriendService;
import com.springapp.mvc.service.image.avatar.AvatarService;
import com.springapp.mvc.service.post.PostService;
import com.springapp.mvc.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by oleh on 11.02.15.
 */
@Controller
public class FriendController {

    @Autowired
    private FriendService friendService;
    @Autowired
    private UserService userService;
    @Autowired
    private PostService postService;
    @Autowired
    private AvatarService avatarService;


    @RequestMapping(value = "/searchFriends", method = RequestMethod.GET)
    public String searchFriends(ModelMap modelMap, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        modelMap.addAttribute("user", user);
        return "searchPage";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@RequestParam String name, ModelMap modelMap, Principal principal) {
        if (name.contains(" ")) {
            String firstName = name.substring(0, name.indexOf(""));
            String lastName = name.substring(name.lastIndexOf(" ") + 1);
            if (firstName.equals("") && lastName.equals("")) {
                return "searchPage";
            } else {
                modelMap.addAttribute("findUsers", userService.getUsers(firstName, lastName));
            }
        } else {
            modelMap.addAttribute("findUsers", userService.getUsers(name, null));
        }
        User user = userService.findUserByEmail(principal.getName());
        modelMap.addAttribute("user", user);
        return "searchPage";
    }

    @RequestMapping(value = "/extraSearching", method = RequestMethod.POST)
    public String extraSearch(@RequestParam String city,
                              @RequestParam String school,
                              @RequestParam String university,
                              ModelMap modelMap,
                              Principal principal) {
        modelMap.addAttribute("findUsers", userService.extraSearching(city, school, university));
        User user = userService.findUserByEmail(principal.getName());
        modelMap.addAttribute("user", user);
        return "searchPage";
    }

    @RequestMapping(value = "/add_to_friends", method = RequestMethod.GET)
    public String addToFriends(@RequestParam int id, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        friendService.createFriend(user.getId(), id);
        return "searchPage";
    }

    @RequestMapping(value = "/confirmation", method = RequestMethod.GET)
    public String updateConfirmation(@RequestParam int userId,
                                     @RequestParam int friendId ) {
        friendService.updateConfirmation(
                friendService.getFriendId(userId, friendId)
        );
        friendService.createFriend(friendId, userId);
        friendService.updateConfirmation(
                friendService.getFriendId(friendId, userId)
        );
        return "redirect:/friends";
    }

    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public String allFriends(Principal principal, ModelMap modelMap) {
        User user = userService.findUserByEmail(principal.getName());
        List<User> userList = new ArrayList<>();
        for (Friend friend : friendService.getFriends(user.getId())) {
            userList.add(userService.getUserInfo(friend.getFriendId()));
        }
        List<User> userRequests = new ArrayList<>();
        for (Friend friend : friendService.getRequests(user.getId())) {
            userRequests.add(userService.getUserInfo(friend.getUserId() ));
        }
        modelMap.addAttribute("listFriends", friendService.getRequests(user.getId()));
        modelMap.addAttribute("requestToFriend", userRequests);
        modelMap.addAttribute("friends", userList);
        modelMap.addAttribute("user", user);
        return "friendsPage";
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public String userPage(ModelMap modelMap, @PathVariable int id, Principal principal) {
        List<Post> posts = postService.getAllPosts(id);
        for (Post post : posts) {
            post.setLikes(postService.getLikes(post.getId()));
        }
        User user = userService.findUserByEmail(principal.getName());
        Avatar avatar = avatarService.getAvatar(id);
        if (avatar != null) {
            modelMap.addAttribute("imageConsist", true);
        } else {
            modelMap.addAttribute("imageConsist", false);
        }
        Collections.sort(posts, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                if (o1.getPostedTime().getTime() < o2.getPostedTime().getTime()) {
                    return 1;
                } else if (o1.getPostedTime().getTime() == o2.getPostedTime().getTime()) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        modelMap.addAttribute("posts", posts);
        modelMap.addAttribute("createAlbum", false);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("id", id);
        return "userPage";
    }

}
