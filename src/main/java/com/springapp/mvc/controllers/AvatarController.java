package com.springapp.mvc.controllers;

import com.springapp.mvc.model.Avatar;
import com.springapp.mvc.model.User;
import com.springapp.mvc.service.image.avatar.AvatarService;
import com.springapp.mvc.service.user.UserService;
import com.springapp.mvc.util.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

/**
 * Created by oleh on 24.02.15.
 */
@Controller
public class AvatarController {

    @Autowired
    private AvatarService avatarService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public String download(
            @ModelAttribute("avatar") Avatar avatar,
            @RequestParam("file") MultipartFile file,
            Principal principal,
            ModelMap modelMap
    ) {
        User user = userService.findUserByEmail(principal.getName());
        try {
            avatarService.removeAvatar(user.getId());
            avatarService.createAvatar(file.getInputStream(), file.getOriginalFilename(),
                    file.getContentType(), user.getId(), file.getSize(), avatar);
        } catch (IOException e) {
            e.printStackTrace();
        }
        modelMap.addAttribute("user", user);
        return "redirect:/settings";
    }

    @RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
    public String showAvatar(@PathVariable final int id, HttpServletResponse response, ModelMap modelMap) {
        Avatar avatar = avatarService.getAvatar(id);
        if (avatar != null) {
            response.setContentType("image/jpeg, image/png, image/jpg");
            Image.writeFileContentHttpResponse(avatar.getContent(), response);
            modelMap.addAttribute("imageConsist", true);
        } else {
            modelMap.addAttribute("imageConsist", false);
        }
        return "redirect:/posts";
    }

}
