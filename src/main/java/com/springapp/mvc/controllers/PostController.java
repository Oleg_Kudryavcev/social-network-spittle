package com.springapp.mvc.controllers;

import com.springapp.mvc.model.Avatar;
import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.User;
import com.springapp.mvc.service.image.avatar.AvatarService;
import com.springapp.mvc.service.post.PostService;
import com.springapp.mvc.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by oleh on 10.02.15.
 */
@Controller
public class PostController {

    @Autowired
    private PostService postService;
    @Autowired
    private AvatarService avatarService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String allPosts(ModelMap modelMap, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        Avatar avatar = avatarService.getAvatar(user.getId());
        if (avatar != null) {
            modelMap.addAttribute("imageConsist", true);
        } else {
            modelMap.addAttribute("imageConsist", false);
        }
        List<Post> posts = postService.getAllPosts(user.getId());
        for (Post post : posts) {
            post.setLikes(postService.getLikes(post.getId()));
        }
        Collections.sort(posts, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                if (o1.getPostedTime().getTime() < o2.getPostedTime().getTime()) {
                    return 1;
                } else if (o1.getPostedTime().getTime() == o2.getPostedTime().getTime()) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        if (user.getBirthDate() != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            dateFormat.format(user.getBirthDate());
        }
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("posts",posts);
        return "profilePage";
    }

    @RequestMapping(value = "/newUserPost/{id}", method = RequestMethod.POST)
    public String newPostUser(@PathVariable int id,
                              Principal principal,
                              @RequestParam String post) {
        User user = userService.findUserByEmail(principal.getName());
        postService.createPost(id, user.getFirstName(), user.getLastName(), post);
        return "redirect:/user/" + id;
    }

    @RequestMapping(value = "/addPost/{id}", method = RequestMethod.POST)
    public String addPost(Principal principal, @RequestParam String post, @PathVariable int id) {
        User user = userService.findUserByEmail(principal.getName());
        postService.createPost(id, user.getFirstName(), user.getLastName(), post);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/like/{postId}", method = RequestMethod.POST)
    public @ResponseBody String like(@PathVariable int postId,
                                     Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        if (postService.getWhoLike(user.getId(), postId) == null) {
            postService.likePost(user.getId(), postId);
        }
        return String.valueOf(postService.getLikes(postId));
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable int id) {
        postService.deletePost(id);
        return "redirect:/posts";
    }

}
