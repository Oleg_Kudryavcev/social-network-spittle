package com.springapp.mvc.config.spring_security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by oleh on 05.03.15.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
