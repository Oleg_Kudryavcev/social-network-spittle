package com.springapp.mvc.dao.user;

import com.springapp.mvc.model.Friend;
import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.User;
import com.springapp.mvc.model.UserRole;

import java.util.Date;
import java.util.List;

/**
 * Created by oleh on 16.01.15.
 */
public interface UserDAO {

    public void addUser(User user);
    public User getUserH(String email, String password);
    public User getUserInfo(int id);
    public List<User> getUsers(String firstName, String lastName);
    public List<User> extraSearching(String city, String school, String university);
    public User findUserByEmail(String email);
    public String getUserRole(String email);
    public void updateUser(User user);
    public boolean userLikePost(int userId);
    public boolean userLikePhoto(int userId);

}
