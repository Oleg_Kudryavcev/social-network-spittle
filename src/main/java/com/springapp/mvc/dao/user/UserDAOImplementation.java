package com.springapp.mvc.dao.user;

import com.springapp.mvc.model.User;
import com.springapp.mvc.model.UserRole;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleh on 06.02.15.
 */
@Repository
public class UserDAOImplementation implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        UserRole userRole = new UserRole();
        userRole.setRole("ROLE_USER");
        userRole.setUserEmail(user.getEmail());
        sessionFactory.getCurrentSession().save(userRole);
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public User getUserH(String email, String password) {
        String hql = "from User user where user.email = :email and user.password = :password";
        return (User) sessionFactory.getCurrentSession().createQuery(hql).setParameter("email", email)
                .setParameter("password", password).uniqueResult();
    }

    @Override
    public User getUserInfo(int id) {
        return (User) sessionFactory.getCurrentSession().createQuery("from User user where user.id = :id")
                .setParameter("id", id).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getUsers(String firstName, String lastName) {
        if (lastName != null) {
            String hql = "from User user where user.firstName = :firstName and user.lastName = :lastName";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("firstName", firstName)
                    .setParameter("lastName", lastName).list();
        } else {
            String hql = "from User user where user.firstName = :firstName or user.lastName = :lastName";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("firstName", firstName)
                    .setParameter("lastName", firstName).list();
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> extraSearching(String city, String school, String university) {
        if ((!city.equals("")) && (!school.equals("")) && (university.equals(""))) {
            String hql = "from User user where user.currentCity = :city and user.school = :school";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("city", city)
                    .setParameter("school", school)
                    .list();
        } else if ((!city.equals("")) && (school.equals("")) && (!university.equals(""))) {
            String hql = "from User user where user.currentCity = :city and user.university = :university";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("city", city)
                    .setParameter("university", university)
                    .list();
        } else if ((city.equals("")) && (!school.equals("")) && (!university.equals(""))) {
            String hql = "from User user where user.school = :school and user.university = :university";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("school", school)
                    .setParameter("university", university)
                    .list();
        } else if ((!city.equals("")) && (school.equals("")) && (university.equals(""))) {
            String hql = "from User user where user.currentCity = :city";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("city", city)
                    .list();
        } else if ((!school.equals("") && (city.equals("")) && (university.equals("")))) {
            String hql = "from User user where user.school = :school";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("school", school)
                    .list();
        } else if ((!university.equals("")) && (city.equals("")) && (school.equals(""))) {
            String hql = "from User user where user.university = :university";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("university", university)
                    .list();
        } else if ((!city.equals("")) && (!school.equals("")) && (!university.equals(""))) {
            String hql = "from User user where user.currentCity = :city or user.school = :school " +
                    "or user.university = :university";
            return sessionFactory.getCurrentSession().createQuery(hql)
                    .setParameter("city", city)
                    .setParameter("school", school)
                    .setParameter("university", university)
                    .list();
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public User findUserByEmail(String email) {
        return (User) sessionFactory.getCurrentSession().createQuery("from User where email = :email")
                .setParameter("email", email).uniqueResult();
    }

    @Override
    public String getUserRole(String email) {
        String hql = "select userRole.role from UserRole userRole where userRole.userEmail = :email";
        return (String) sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("email", email).uniqueResult();
    }

    @Override
    public void updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public boolean userLikePost(int userId) {
        String hql = "from PostLike where userWhoLikeId = :userId";
        Object user = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId).uniqueResult();
        return user != null;
    }

    @Override
    public boolean userLikePhoto(int userId) {
        String hql = "from PhotoLike where whoLikeId = :userId";
        Object user = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId).uniqueResult();
        return user != null;
    }
}
