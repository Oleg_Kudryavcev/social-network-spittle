package com.springapp.mvc.dao.image.albom;

import com.springapp.mvc.model.AlbumPhoto;
import com.springapp.mvc.model.PhotoComment;
import com.springapp.mvc.model.PhotoLike;
import com.springapp.mvc.model.PostLike;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by oleh on 25.02.15.
 */
public interface AlbumPhotoDAO {

    public AlbumPhoto getAlbumPhotoById(int userId, int photoId);
    public List<AlbumPhoto> getAllUserPhotos(int id);
    public void savePhoto(AlbumPhoto albumPhoto);
    public void removePhoto(AlbumPhoto albumPhoto);
    public SessionFactory getSessionFactory();
    public PhotoLike getWhoLike(int userId, int postId);
    public void savePhotoComment(PhotoComment photoComment);
    public List<PhotoComment> allPhotoComments(int photoId);
    public void likePhoto(int userId, int postId);
    public int getLikes(int photoId);

}
