package com.springapp.mvc.dao.image.avatar;

import com.springapp.mvc.dao.image.avatar.AvatarDAO;
import com.springapp.mvc.model.Avatar;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Blob;

/**
 * Created by oleh on 24.02.15.
 */
@Repository
public class AvatarDAOImplementation implements AvatarDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public Avatar getAvatar(int id) {
        String sql = "FROM Avatar avatar WHERE avatar.userId = :id";
        return (Avatar) sessionFactory.getCurrentSession()
                .createQuery(sql).setParameter("id", id).uniqueResult();
    }

    @Override
    public void saveAvatar(Avatar avatar) {
        sessionFactory.getCurrentSession().save(avatar);
    }

    @Override
    public void removeAvatar(int id) {
        String hql = "delete from Avatar where userId = :id";
        sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public Blob getAvatarByUserId(int userId) {
        String sql = "SELECT IMAGE FROM Avatar WHERE userId = :userId";
        return (Blob) sessionFactory.getCurrentSession().createQuery(sql)
                .setParameter("userId", userId).uniqueResult();
    }


}
