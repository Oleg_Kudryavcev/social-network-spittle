package com.springapp.mvc.dao.image.albom;

import com.springapp.mvc.model.Album;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
@Repository
public class AlbumInfoDAOImplementation implements AlbumInfoDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Album> getAlbums(int userId) {
        String sql = "from Album where userId = :userId";
        return sessionFactory.getCurrentSession().createQuery(sql)
                .setParameter("userId", userId).list();
    }

    @Override
    public void saveAlbum(Album album) {
        sessionFactory.getCurrentSession().save(album);
    }

    @Override
    public void editAlbum(Album album) {
        sessionFactory.getCurrentSession().update(album);
    }

    @Override
    public void removeAlbum(Album album) {
        sessionFactory.getCurrentSession().delete(album);
    }

    @Override
    public Album getAlbumById(int id) {
        return (Album) sessionFactory.getCurrentSession().get(Album.class, id);
    }

}
