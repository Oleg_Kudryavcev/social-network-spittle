package com.springapp.mvc.dao.image.albom;

import com.springapp.mvc.model.Album;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
public interface AlbumInfoDAO {

    public List<Album> getAlbums(int userId);
    public void saveAlbum(Album album);
    public void editAlbum(Album album);
    public void removeAlbum(Album album);
    public Album getAlbumById(int id);

}
