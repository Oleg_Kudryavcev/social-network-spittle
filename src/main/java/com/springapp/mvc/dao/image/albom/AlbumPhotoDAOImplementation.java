package com.springapp.mvc.dao.image.albom;

import com.springapp.mvc.model.AlbumPhoto;
import com.springapp.mvc.model.PhotoComment;
import com.springapp.mvc.model.PhotoLike;
import com.springapp.mvc.model.PostLike;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by oleh on 25.02.15.
 */
@Repository
public class AlbumPhotoDAOImplementation implements AlbumPhotoDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public PhotoLike getWhoLike(int userId, int postId) {
        String hql = "from PhotoLike where whoLikeId = :userId and photoId = :postId";
        return (PhotoLike) sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId)
                .setParameter("postId", postId)
                .uniqueResult();
    }

    @Override
    public void likePhoto(int userId, int postId) {
        sessionFactory.getCurrentSession().save(new PhotoLike(userId, postId));
    }

    @SuppressWarnings("unchecked")
    @Override
    public int getLikes(int photoId) {
        String hql = "from PhotoLike where photoId = :photoId";
        List<PhotoLike> photoLikes = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("photoId", photoId).list();
        return photoLikes.size();
    }

    @Override
    public void savePhotoComment(PhotoComment photoComment) {
        sessionFactory.getCurrentSession().save(photoComment);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PhotoComment> allPhotoComments(int photoId) {
        String sql = "from PhotoComment where photoId = :photoId";
        return sessionFactory.getCurrentSession().createQuery(sql)
                .setParameter("photoId", photoId).list();
    }

    @Override
    public AlbumPhoto getAlbumPhotoById(int userId, int photoId) {
        String sql = "from AlbumPhoto photo where photo.whomPhotoId = :userId and photo.id = :photoId";
        return (AlbumPhoto) sessionFactory.getCurrentSession().createQuery(sql)
                .setParameter("userId", userId).setParameter("photoId", photoId).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AlbumPhoto> getAllUserPhotos(int id) {
        String sql = "from AlbumPhoto where albumId = :id";
        return sessionFactory.getCurrentSession().createQuery(sql)
                .setParameter("id", id).list();
    }

    @Override
    public void savePhoto(AlbumPhoto albumPhoto) {
        sessionFactory.getCurrentSession().save(albumPhoto);
    }

    @Override
    public void removePhoto(AlbumPhoto albumPhoto) {
        sessionFactory.getCurrentSession().delete(albumPhoto);
    }
}
