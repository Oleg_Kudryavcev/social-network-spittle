package com.springapp.mvc.dao.image.avatar;

import com.springapp.mvc.model.Avatar;
import org.hibernate.SessionFactory;

import java.sql.Blob;

/**
 * Created by oleh on 24.02.15.
 */
public interface AvatarDAO {

    public void saveAvatar(Avatar avatar);
    public void removeAvatar(int id);
    public Blob getAvatarByUserId(int userId);
    public SessionFactory getSessionFactory();
    public Avatar getAvatar(int id);

}
