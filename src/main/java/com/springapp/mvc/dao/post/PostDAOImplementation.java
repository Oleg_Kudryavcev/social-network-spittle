package com.springapp.mvc.dao.post;

import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.PostLike;
import com.springapp.mvc.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
@Repository
public class PostDAOImplementation implements PostDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addPost(Post post) {
        sessionFactory.getCurrentSession().save(post);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Post> getAllPosts(int id) {
        String hql = "from Post post where post.user_id = :user_id";
        return sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("user_id", id).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public int getLikes(int postId) {
        String hql = "from PostLike where postId = :postId";
        List<PostLike> postLikes = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("postId", postId).list();
        return postLikes.size();
    }

    @Override
    public int deletePost(int id) {
        String hql = "delete from Post where id = :id";
        return sessionFactory.getCurrentSession().createQuery(hql).setParameter("id", id).executeUpdate();
    }

    @Override
    public void likePost(int userId, int postId) {
        sessionFactory.getCurrentSession().save(new PostLike(userId, postId));
    }

    @Override
    public PostLike getWhoLike(int userId, int postId) {
        String hql = "from PostLike where userWhoLikeId = :userId and postId = :postId";
        return (PostLike) sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId)
                .setParameter("postId", postId)
                .uniqueResult();
    }

}
