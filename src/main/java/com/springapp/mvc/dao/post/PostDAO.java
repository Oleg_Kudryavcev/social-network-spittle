package com.springapp.mvc.dao.post;

import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.PostLike;
import com.springapp.mvc.model.User;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
public interface PostDAO {

    public void addPost(Post post);
    public List<Post> getAllPosts(int id);
    public int getLikes(int postId);
    public int deletePost(int id);
    public void likePost(int userId, int postId);
    public PostLike getWhoLike(int userId, int postId);

}
