package com.springapp.mvc.dao.friend;

import com.springapp.mvc.model.Friend;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
public interface FriendDAO {

    public void addToFriends(Friend friend);
    public List<Friend> getFriends(int userId);
    public void updateConfirmation(int id);
    public List<Friend> getRequests(int userId);
    public int getFriendId(int userId, int friendId);

}
