package com.springapp.mvc.dao.friend;

import com.springapp.mvc.model.Friend;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
@Repository
public class FriendDAOImplementation implements FriendDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void updateConfirmation(int id) {
        String hql = "update Friend set confirmation = true where id = :id";
        sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Friend> getRequests(int userId) {
        String hql = "from Friend friend where friend.friendId = :userId and confirmation = false";
        return sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId).list();
    }

    @Override
    public int getFriendId(int userId, int friendId) {
        String hql = "select id from Friend friend where friend.userId = :userId and friend.friendId = :friendId";
        return (int) sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId)
                .setParameter("friendId", friendId)
                .uniqueResult();
    }

    @Override
    public void addToFriends(Friend friend) {
        sessionFactory.getCurrentSession().save(friend);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Friend> getFriends(int userId) {
        String hql = "from Friend friend where friend.userId = :userId and friend.confirmation = true";
        return sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("userId", userId).list();
    }

}
