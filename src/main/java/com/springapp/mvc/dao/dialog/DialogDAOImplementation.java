package com.springapp.mvc.dao.dialog;

import com.springapp.mvc.dao.user.UserDAO;
import com.springapp.mvc.model.Dialog;
import com.springapp.mvc.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.*;

/**
 * Created by oleh on 17.02.15.
 */
@Repository
public class DialogDAOImplementation implements DialogDAO {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private UserDAO userDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getDialogs(int id) {
        List<Integer> dialogs = new ArrayList<>();
        String hql = "select whoAnswerId from Dialog where whoWriteId = :whoWriteId";
        dialogs.addAll(
                sessionFactory.getCurrentSession().createQuery(hql)
                        .setParameter("whoWriteId", id).list()
        );
        Set<Integer> dialogSet = new LinkedHashSet<>();
        dialogSet.addAll(dialogs);
        List<User> users = new LinkedList<>();
        for (Integer userId : dialogSet) {
            users.add(userDAO.getUserInfo(userId));
        }
        return users;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Dialog> allDialogsWithFriend(int userId, int friendId) {
        String hql = "from Dialog dialog where dialog.whoWriteId = :whoWriteId and dialog.whoAnswerId = :whoAnswerId";
        List<Dialog> list = new LinkedList<>();
        list.addAll(sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("whoWriteId", userId)
                .setParameter("whoAnswerId", friendId)
                .list());
        list.addAll(sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("whoWriteId", friendId)
                .setParameter("whoAnswerId", userId)
                .list());
        return list;
    }

    @Override
    public void addMessage(Dialog dialog) {
        sessionFactory.getCurrentSession().save(dialog);
    }
}
