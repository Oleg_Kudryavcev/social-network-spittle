package com.springapp.mvc.util;

/**
 * Created by oleh on 17.03.15.
 */
public class Message {

    private String message;
    private int friendId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }
}
