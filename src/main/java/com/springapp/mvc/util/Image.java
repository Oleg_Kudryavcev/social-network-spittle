package com.springapp.mvc.util;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by oleh on 25.02.15.
 */
public class Image {

    public static void writeFileContentHttpResponse(final Blob image, final HttpServletResponse response) {
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            InputStream inputStream = image.getBinaryStream();
            int length;
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
            inputStream.close();
            outputStream.flush();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

}
