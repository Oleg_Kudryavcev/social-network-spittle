package com.springapp.mvc.service.user;

import com.springapp.mvc.dao.user.UserDAO;
import com.springapp.mvc.model.User;
import com.springapp.mvc.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * Created by oleh on 06.02.15.
 */
@Service
@Transactional
public class UserServiceImplementation implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public void addUser(User user) {
        userDAO.addUser(user);
    }

    @Override
    public void createUser(String firstName, String lastName, String password, String email) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setEmail(email);
        addUser(user);
    }

    @Override
    public User getUserH(String email, String password) {
        return userDAO.getUserH(email, password);
    }

    @Override
    public User getUserInfo(int id) {
        return userDAO.getUserInfo(id);
    }

    @Override
    public List<User> getUsers(String firstName, String lastName) {
        return userDAO.getUsers(firstName, lastName);
    }

    @Override
    public List<User> extraSearching(String city, String school, String university) {
        return userDAO.extraSearching(city, school, university);
    }

    @Override
    public User findUserByEmail(String email) {
        return userDAO.findUserByEmail(email);
    }

    @Override
    public String getUserRole(String email) {
        return userDAO.getUserRole(email);
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }

    @Override
    public boolean userLikePost(int userId) {
        return userDAO.userLikePost(userId);
    }

    @Override
    public boolean userLikePhoto(int userId) {
        return userDAO.userLikePhoto(userId);
    }
}
