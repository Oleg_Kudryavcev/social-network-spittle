package com.springapp.mvc.service.post;

import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.PostLike;
import com.springapp.mvc.model.User;

import java.util.Date;
import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
public interface PostService {

    public void addPost(Post post);
    public List<Post> getAllPosts(int id);
    public void createPost(int id, String firstName, String lastName, String text);
    public int deletePost(int id);
    public int getLikes(int postId);
    public void likePost(int userId, int postId);
    public PostLike getWhoLike(int userId, int postId);

}
