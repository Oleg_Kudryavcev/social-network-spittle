package com.springapp.mvc.service.post;

import com.springapp.mvc.dao.post.PostDAO;
import com.springapp.mvc.model.Post;
import com.springapp.mvc.model.PostLike;
import com.springapp.mvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
@Service
@Transactional
public class PostServiceImplementation implements PostService {

    @Autowired
    private PostDAO postDAO;

    @Override
    public void addPost(Post post) {
        postDAO.addPost(post);
    }

    @Override
    public List<Post> getAllPosts(int id) {
        return postDAO.getAllPosts(id);
    }

    @Override
    public void createPost(int id, String firstName, String lastName, String text) {
        Post post = new Post();
        post.setWhoPosted(firstName + " " + lastName);
        post.setPost(text);
        post.setUser_id(id);
        addPost(post);
    }

    @Override
    public int deletePost(int id) {
        return postDAO.deletePost(id);
    }

    @Override
    public int getLikes(int postId) {
        return postDAO.getLikes(postId);
    }

    @Override
    public void likePost(int userId, int postId) {
        postDAO.likePost(userId, postId);
    }

    @Override
    public PostLike getWhoLike(int userId, int postId) {
        return postDAO.getWhoLike(userId, postId);
    }

}
