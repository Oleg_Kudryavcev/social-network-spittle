package com.springapp.mvc.service.image.album;

import com.springapp.mvc.dao.image.albom.AlbumInfoDAO;
import com.springapp.mvc.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
@Service
@Transactional
public class AlbumInfoServiceImplementation implements AlbumInfoService {

    @Autowired
    private AlbumInfoDAO albumInfoDAO;

    @Override
    public List<Album> getAlbums(int userId) {
        return albumInfoDAO.getAlbums(userId);
    }

    @Override
    public void saveAlbum(Album album) {
        albumInfoDAO.saveAlbum(album);
    }

    @Override
    public void editAlbum(Album album) {
        albumInfoDAO.editAlbum(album);
    }

    @Override
    public void removeAlbum(Album album) {
        albumInfoDAO.removeAlbum(album);
    }

    @Override
    public void createAlbum(String albumName, int userId, String whoCanComment) {
        Album album = new Album();
        album.setWhoCanComment(whoCanComment);
        album.setAlbumName(albumName);
        album.setUserId(userId);
        saveAlbum(album);
    }

    @Override
    public Album getAlbumById(int id) {
        return albumInfoDAO.getAlbumById(id);
    }
}
