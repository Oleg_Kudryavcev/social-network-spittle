package com.springapp.mvc.service.image.album;

import com.springapp.mvc.dao.image.albom.AlbumPhotoDAO;
import com.springapp.mvc.model.AlbumPhoto;
import com.springapp.mvc.model.PhotoComment;
import com.springapp.mvc.model.PhotoLike;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.sql.Blob;
import java.util.List;

/**
 * Created by oleh on 25.02.15.
 */
@Service
@Transactional
public class AlbumPhotoServiceImplementation implements AlbumPhotoService {

    @Autowired
    private AlbumPhotoDAO albumPhotoDAO;


    @Override
    public AlbumPhoto getAlbumPhotoById(int userId, int photoId) {
        return albumPhotoDAO.getAlbumPhotoById(userId, photoId);
    }

    @Override
    public List<AlbumPhoto> getAllUserPhotos(int id) {
        return albumPhotoDAO.getAllUserPhotos(id);
    }

    @Override
    public void savePhoto(AlbumPhoto albumPhoto) {
        albumPhotoDAO.savePhoto(albumPhoto);
    }

    @Override
    public void removePhoto(AlbumPhoto albumPhoto) {
        albumPhotoDAO.removePhoto(albumPhoto);
    }

    @Override
    public void createAlbumPhoto(InputStream inputStream, String description,
                                 String contentType, int userId, int albumId,
                                 long fileSize, AlbumPhoto albumPhoto) {
        Blob blob = Hibernate.getLobCreator(albumPhotoDAO.getSessionFactory().getCurrentSession())
                .createBlob(inputStream, fileSize);
        albumPhoto.setAlbumId(albumId);
        albumPhoto.setContentType(contentType);
        albumPhoto.setDescription(description);
        albumPhoto.setImage(blob);
        albumPhoto.setWhomPhotoId(userId);
        savePhoto(albumPhoto);
    }

    @Override
    public List<PhotoComment> allPhotoComments(int photoId) {
        return albumPhotoDAO.allPhotoComments(photoId);
    }

    @Override
    public void createPhotoComment(String comment, int whoCommentId, int photoId, String whoCommentName) {
        PhotoComment photoComment = new PhotoComment();
        photoComment.setComment(comment);
        photoComment.setPhotoId(photoId);
        photoComment.setWhoCommentId(whoCommentId);
        photoComment.setNameWhoComment(whoCommentName);
        albumPhotoDAO.savePhotoComment(photoComment);
    }

    @Override
    public void likePhoto(int userId, int photoId) {
        albumPhotoDAO.likePhoto(userId, photoId);
    }

    @Override
    public PhotoLike getWhoLike(int userId, int photoId) {
        return albumPhotoDAO.getWhoLike(userId, photoId);
    }

    @Override
    public int getLikes(int photoId) {
        return albumPhotoDAO.getLikes(photoId);
    }
}
