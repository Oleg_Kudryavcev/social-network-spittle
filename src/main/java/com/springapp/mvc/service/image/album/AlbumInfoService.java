package com.springapp.mvc.service.image.album;

import com.springapp.mvc.model.Album;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
public interface AlbumInfoService {

    public List<Album> getAlbums(int userId);
    public void saveAlbum(Album album);
    public void editAlbum(Album album);
    public void removeAlbum(Album album);
    public void createAlbum(String albumName, int userId, String whoCanComment);
    public Album getAlbumById(int id);

}
