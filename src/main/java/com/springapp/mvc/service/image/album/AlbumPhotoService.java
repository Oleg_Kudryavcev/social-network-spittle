package com.springapp.mvc.service.image.album;

import com.springapp.mvc.model.AlbumPhoto;
import com.springapp.mvc.model.PhotoComment;
import com.springapp.mvc.model.PhotoLike;
import org.hibernate.SessionFactory;

import java.io.InputStream;
import java.util.List;

/**
 * Created by oleh on 25.02.15.
 */
public interface AlbumPhotoService {

    public AlbumPhoto getAlbumPhotoById(int userId, int photoId);
    public List<AlbumPhoto> getAllUserPhotos(int id);
    public void savePhoto(AlbumPhoto albumPhoto);
    public void removePhoto(AlbumPhoto albumPhoto);
    public void createAlbumPhoto(InputStream inputStream, String description,
                                 String contentType, int userId, int albumId,
                                 long fileSize, AlbumPhoto albumPhoto);
    public List<PhotoComment> allPhotoComments(int photoId);
    public void createPhotoComment(String comment, int whoCommentId, int photoId, String whoCommentName);
    public void likePhoto(int userId, int photoId);
    public PhotoLike getWhoLike(int userId, int photoId);
    public int getLikes(int photoId);

}
