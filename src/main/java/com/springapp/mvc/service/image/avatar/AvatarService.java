package com.springapp.mvc.service.image.avatar;

import com.springapp.mvc.model.Avatar;

import java.io.InputStream;
import java.sql.Blob;

/**
 * Created by oleh on 24.02.15.
 */
public interface AvatarService {

    public void saveAvatar(Avatar avatar);
    public void removeAvatar(int id);
    public void createAvatar(InputStream inputStream, String fileName,
                             String contentType, int userId,
                             long fileSize, Avatar avatar);
    public Avatar getAvatar(int id);

}
