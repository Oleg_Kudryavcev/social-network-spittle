package com.springapp.mvc.service.image.avatar;

import com.springapp.mvc.dao.image.avatar.AvatarDAO;
import com.springapp.mvc.model.Avatar;
import com.springapp.mvc.service.image.avatar.AvatarService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.sql.Blob;

/**
 * Created by oleh on 24.02.15.
 */
@Service
@Transactional
public class AvatarServiceImplementation implements AvatarService {

    @Autowired
    private AvatarDAO avatarDAO;

    @Override
    public void saveAvatar(Avatar avatar) {
        avatarDAO.saveAvatar(avatar);
    }

    @Override
    public void removeAvatar(int id) {
        avatarDAO.removeAvatar(id);
    }

    @Override
    public void createAvatar(InputStream inputStream, String fileName,
                             String contentType, int userId,
                             long fileSize, Avatar avatar) {
        Blob blob = Hibernate.getLobCreator(avatarDAO.getSessionFactory()
                .getCurrentSession()).createBlob(inputStream, fileSize);
        avatar.setFileName(fileName);
        avatar.setUserId(userId);
        avatar.setContent(blob);
        avatar.setContentType(contentType);
        saveAvatar(avatar);
    }

    @Override
    public Avatar getAvatar(int id) {
        return avatarDAO.getAvatar(id);
    }
}
