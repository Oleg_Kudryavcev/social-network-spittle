package com.springapp.mvc.service.dialog;

import com.springapp.mvc.model.Dialog;
import com.springapp.mvc.model.User;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by oleh on 17.02.15.
 */
public interface DialogService {

    public List<User> getDialogs(int id);

    public List<Dialog> allDialogsWithFriend(int userId, int friendId);

    public void createMessage(int userId, int friendId, String message);

}
