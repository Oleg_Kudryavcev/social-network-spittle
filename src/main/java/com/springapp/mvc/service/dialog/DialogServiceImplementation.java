package com.springapp.mvc.service.dialog;

import com.springapp.mvc.dao.dialog.DialogDAO;
import com.springapp.mvc.model.Dialog;
import com.springapp.mvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by oleh on 17.02.15.
 */
@Service
@Transactional
public class DialogServiceImplementation implements DialogService {

    @Autowired
    private DialogDAO dialogDAO;

    @Override
    public List<User> getDialogs(int id) {
        return dialogDAO.getDialogs(id);
    }

    @Override
    public List<Dialog> allDialogsWithFriend(int userId, int friendId) {
        return dialogDAO.allDialogsWithFriend(userId,friendId);
    }

    @Override
    public void createMessage(int userId, int friendId, String message) {
        Dialog dialog = new Dialog();
        dialog.setWhoWriteId(userId);
        dialog.setWhoAnswerId(friendId);
        dialog.setMessage(message);
        dialogDAO.addMessage(dialog);
    }
}
