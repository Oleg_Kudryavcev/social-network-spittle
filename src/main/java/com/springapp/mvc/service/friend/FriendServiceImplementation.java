package com.springapp.mvc.service.friend;

import com.springapp.mvc.dao.friend.FriendDAO;
import com.springapp.mvc.model.Friend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by oleh on 24.02.15.
 */
@Service
@Transactional
public class FriendServiceImplementation implements FriendService {

    @Autowired
    private FriendDAO friendDAO;

    @Override
    public void createFriend(int userId, int friendId) {
        Friend friend = new Friend();
        friend.setUserId(userId);
        friend.setFriendId(friendId);
        addToFriends(friend);
    }

    @Override
    public void addToFriends(Friend friend) {
        friendDAO.addToFriends(friend);
    }

    @Override
    public List<Friend> getFriends(int userId) {
        return friendDAO.getFriends(userId);
    }

    @Override
    public void updateConfirmation(int id) {
        friendDAO.updateConfirmation(id);
    }

    @Override
    public List<Friend> getRequests(int userId) {
        return friendDAO.getRequests(userId);
    }

    @Override
    public int getFriendId(int userId, int friendId) {
        return friendDAO.getFriendId(userId, friendId);
    }

}
